<?php

class Helpers {
    static public function parrotHeaders() {
        $parrotHeaders = array(
            "HTTP_X_REQUEST_ID" => "x-request-id",
            "HTTP_X_B3_TRACEID" => "x-b3-traceid",
            "HTTP_X_B3_SPANID" => "x-b3-spanid",
            "HTTP_X_B3_PARENTSPANID" => "x-b3-parentspanid" ,
            "HTTP_X_B3_SAMPLED" => "x-b3-sampled",
            "HTTP_X_B3_FLAGS" => "x-b3-flags",
            "HTTP_X_OT_SPAN_CONTEXT" => "x-ot-span-context"
        );
        $returnHeaders = array();

        error_log("I'm going to send out requests, and here's the headers I'm parroting:");
        foreach ($parrotHeaders as $key => $value) {
            if (!empty($_SERVER[$key])) {
                array_push($returnHeaders, $value . ": " . $_SERVER["$key"]);
                error_log($value . ": " . $_SERVER["$key"]);
            }
        }
        error_log("End of parroting");
        return $returnHeaders;
    }

    static public function doCurlRequest($URL) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, static::parrotHeaders());

        return curl_exec($ch);
    }

    static public function doMultiCurlRequest($URLs) {
        $coreCh = curl_multi_init();
        $chArray = array();
        $headers = static::parrotHeaders();

        for ($x = 0; $x < sizeof($URLs); $x++) {
            $chArray[$x] = curl_init();
            curl_setopt($chArray[$x], CURLOPT_URL,$URLs[$x]);
            curl_setopt($chArray[$x], CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($chArray[$x], CURLOPT_HEADER, TRUE);
            curl_setopt($chArray[$x], CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($chArray[$x], CURLOPT_TIMEOUT, 60);
            curl_setopt($chArray[$x], CURLOPT_MAXCONNECTS, 30);
            curl_setopt($chArray[$x], CURLOPT_HTTPHEADER, $headers);
            curl_multi_add_handle($coreCh, $chArray[$x]);
        }

        curl_multi_exec($coreCh,$running);
        while ($running > 0) {
            usleep(2000);
            curl_multi_exec($coreCh,$running);
        }

        $output = array();
        foreach($chArray as $entry) {
            $rawEntryOutput = curl_multi_getcontent($entry);
            array_push($output, substr($rawEntryOutput, curl_getinfo($entry, CURLINFO_HEADER_SIZE)));
        }

        return $output;
    }
}
