<?php

require_once __DIR__ . "/functions.php";

$urls = array(
    "http://dewey/api/v1/dewey.php",
);

\Helpers::doMultiCurlRequest($urls);

$delay = mt_rand() / mt_getrandmax() * 10000;
usleep($delay);

echo "Done fetching data!\n";