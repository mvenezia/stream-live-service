<?php

require_once __DIR__ . "/functions.php";

$urls = array(
    "http://huey/api/v1/huey.php",
    "http://dewey/api/v1/dewey.php",
    "http://louie/api/v1/louie.php",
);

\Helpers::doMultiCurlRequest($urls);

$delay = mt_rand() / mt_getrandmax() * 20000;
usleep($delay);

echo "Done fetching data!\n";